<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'libraries/mail.php';

class Admin_Controller extends Ci_Controller {

    /**
     * Data Input
     */
    protected $_input;

    function __construct(){

        parent::__construct();

        session_start();

        $this->load->helper('flash');
        $this->load->helper('datetime');
        $this->load->helper('my_form');

        // Obtiene las instancias
        $ci =& get_instance();

        if($this->session->userdata('login') == NULL OR $this->session->userdata('login') == '') {
            if($ci->router->fetch_class() != 'sessions') {
                return redirect('cmsadmin/sessions/login/');
            }
        }

        $this->load->library('utils');
        $this->load->model('Api');
        require_once APPPATH.'core/active_record.php';
        $this->load->model('User_model');
        //$this->load->model('Topic_model');
        //$this->load->model('Task_model');
        //$this->load->model('Answer_model');
        //$this->load->model('Comment_model');
        //$this->load->model('Abuse_report_model');

        $this->_input   = isset($_POST) ? file_get_contents("php://input") : array();

    }


    /**
     * Upload file
     */
    public function upload($input='none', $file='') {
        $config['upload_path']      = './files/';
        if($file) {
            $config['allowed_types']    = '*';
            $config['encrypt_name']     = false;
        } else {
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['encrypt_name']     = true;
        }
        $config['max_size']         = '100000';
        $data                       = array();
        $data['error']              = TRUE;
        $this->load->library('upload', $config);
        if($this->upload->do_upload($input)) {
            $filedata = $this->upload->data();
            chmod("{$filedata['file_path']}{$filedata['file_name']}", 0777);
            $resize = $this->input->get('resize');
            $size   = empty($rezise) ? explode('x', strtolower($resize)) : NULL;
            $w      = empty($size[0]) ? 0 : $size[0];
            $h      = empty($size[1]) ? 0 : $size[1];
            $force  = empty($this->input->get('force')) ? FALSE : TRUE;
            if($w && $h) {
                require_once APPPATH . 'libraries/wideimage/WideImage.php';
                if($force) {
                    WideImage::load($filedata['file_path'].$filedata['file_name'])->resize($w, $h, 'fill', 'any')->saveToFile($filedata['file_path'].$filedata['file_name']);
                } else {
                    WideImage::load($filedata['file_path'].$filedata['file_name'])->resize($w, $h, 'inside', 'down')->saveToFile($filedata['file_path'].$filedata['file_name']);
                }
            }
            if(is_null(CDN_FILES)) { //Localhost
                $data['error']  = FALSE;
                $data['source'] = '/files/';
                $data['name']   = $filedata['file_name'];
            } else { //Rackspace
                try {
                    $localfile  = $filedata['file_path'].'./'.$filedata['file_name'];
                    $this->load->library('opencloud');
                    $this->opencloud->set_container(CDN_CONTAINER);
                    if($this->opencloud->add_object("files/{$filedata['file_name']}", $localfile)) {
                        $data['source']     = CDN_FILES."/files/";
                        $data['error']      = FALSE;
                        $data['name']       = $filedata['file_name'];
                    } else {
                        $data['error']      = TRUE;
                        $data['message']    = $this->opencloud->get_last_error();
                    }
                } catch(Exception $e) {
                    $data['error']      = TRUE;
                    $data['message']    = $e->getMessage();
                }
            }
        } else {
            $data['message']    = $this->upload->display_errors('', '');
        }
        $this->load->view('partials/json', array('data'=>$data));
    }



    /**
     * Method to load pagination
     * @param type $url
     * @param type $record
     */
    protected function _pagination($url, $record) {

        //Pagination
        $config['base_url']             = $url;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = empty($record->size) ? 0 : $record->size;
        $config['per_page']             = PER_PAGE;
        $config['num_links']            = 10;
        $config['next_link']            = '<i class="fa fa-angle-right"></i>';
        $config['prev_link']            = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open']        = '<li class="arrow">';
        $config['prev_tag_close']       = '</li>';
        $config['next_tag_open']        = '<li class="arrow">';
        $config['next_tag_close']       = '</li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';
        $config['cur_tag_open']         = '<li class="current"><a href="#" class="disabled">';
        $config['cur_tag_close']        = '</a></li>';
        $config['anchor_class']         = ' class="js-load" ';

        $this->pagination->initialize($config);

    }

}
