<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'libraries/mail.php';

class Front_Controller extends Ci_Controller {

    /**
     * Data Input
     */
    protected $_input;

    function __construct(){

        parent::__construct();
        session_start();

        $this->load->model('Api');
        require_once APPPATH.'core/active_record.php';

        $this->load->library('session');

        $this->load->helper('flash');
        $this->load->helper('datetime');
        $this->load->helper('my_form');

        $this->load->library('session');
        $this->load->library('mobile_detect');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('utils');
        $this->load->model('User_model');


        $this->_input   = isset($_POST) ? file_get_contents("php://input") : array();

    }

}
