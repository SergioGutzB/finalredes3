<?php

class Active_record extends CI_Model {

    /**
     * Status Active
     */
    const ACTIVE = 1;

    /**
     * Status Deleted
     */
    const DELETED = 0;

    /**
     * Name table
     * @var string
     */
    protected $_source;

    /**
     * Fields
     */
    protected $_fields;

    /**
     * Construct
     */
    public function __construct($data=array()) {
        parent::__construct();
        $this->_modelName();
        $this->_dump();
        $this->_dumpResultSelf($data);
        //$this->db->db_debug = FALSE;
    }

    /**
     * Get name from relation of RDBM based in the class name
     */
    protected function _modelName() {
        $this->_source  = strtolower(preg_replace('/_model/', "", get_class($this)));
    }

    /**
     * Dump
     */
    protected function _dump() {
        $this->_fields  = $this->db->field_data($this->_source);
    }

    /**
     * Autoload object
     * @param type $data
     */
    protected function _dumpResultSelf($data) {
        if (is_array($data) || is_object($data)) {
            foreach ($data as $k => $r) {
                if (!is_numeric($k)) {
                    $this->$k = (is_array($r)) ? $r : stripslashes($r);
                }
            }
        }
    }

    /**
     *
     * @param array $result
     * @return ActiveRecord
     */
    public function _dumpResult($result) {
        $obj = clone $this;
        if (is_array($result) || is_object($result)) {
            foreach ($result as $k => $r) {
                if (!is_numeric($k)) {
                    $obj->$k = stripslashes($r);
                }
            }
        }
        return $obj;
    }

    /**
     * Begin transacction
     */
    public function begin() {
        $this->db->trans_begin();
    }

    /**
     * Commit transacction
     */
    public function commit() {
        $this->db->trans_commit();
    }

    /**
     * Rollback transacction
     */
    public function rollback() {
        $this->db->trans_rollback();
    }

    /**
     * Last Query
     */
    public function lastQuery() {
        return $this->db->last_query();
    }

    /**
     * Method to save
     */
    public function create() {
        if(isset($this->id)) {
            $this->id   = NULL;
        }
        return $this->save();
    }

    /**
     * Method to update
     */
    public function update() {
        if(empty($this->id)) {
            Flash::error('Cannot be updated, because the record does not exist');
            return FALSE;
        }
        return $this->save();
    }

    /**
     * Return First Record
     *
     * @param mixed $what
     * @return ActiveRecord Cursor
     */
    public function first($what = '') {
        if(is_numeric($what)) {
            $this->db->where('id', $what);
        } else if(is_array($what)) {
            $this->db->where($what);
        } else {
            $what = self::getParams(func_get_args());
            $this->convertParams($what);
        }
        $this->db->limit(1);
        $result = $this->db->get($this->_source)->result();
        $resp   = NULL;
        if(!empty($result[0])) {
            $this->_dumpResultSelf($result[0]);
            $resp = $this->_dumpResult($result[0]);
        }
        return $resp;
    }

    /**
     * Return All Record
     *
     * @param mixed $what
     * @return ActiveRecord Cursor
     */
    public function find($what = '') {
        if(is_numeric($what)) {
            $this->db->where('id', $what);
        } else if(is_array($what)) {
            $this->db->where($what);
        } else {
            $what = self::getParams(func_get_args());
            $this->convertParams($what);
        }
        $results        = array();
        $all_results    = $this->db->get($this->_source)->result();
        foreach ($all_results AS $result) {
            $results[]  = $this->_dumpResult($result);
        }

        return $results;
    }

    /**
     * Count All Record
     *
     * @param mixed $what
     * @return int
     */
    public function count($what = '') {
        if(is_numeric($what)) {
            $this->db->where('id', $what);
        } else if(is_array($what)) {
            $this->db->where($what);
        } else {
            $what = self::getParams(func_get_args());
            $table = $this->_source;
            if(isset($what['order'])) {
                unset($what['order']);
            }
            if(isset($what['group'])) {
                $select = "SELECT COUNT(*) as rows FROM (SELECT $table.* FROM $table ";
                $select.= $this->convertParams($what, TRUE);
                $select.=') AS t';
                $q      = $this->db->query($select);
                $result = $q->result();
                return empty($result[0]->rows) ? 0 : $result[0]->rows;
            } else {
                $this->convertParams($what);
            }
        }
        $q = $this->db->count_all_results($this->_source);
        return (empty($q)) ? 0 : $q;
    }

    /**
     * Save
     */
    public function save() {

        if(empty($this->id)) {
            if (method_exists($this, "_beforeCreate")) {
                if ($this->_beforeCreate() == 'cancel') {
                    return FALSE;
                }
            }
        } else {
            if (method_exists($this, "_beforeUpdate")) {
                if ($this->_beforeCreate() == 'cancel') {
                    return FALSE;
                }
            }
        }

        if (method_exists($this, "_beforeSave")) {
            if ($this->_beforeSave() == 'cancel') {
                return FALSE;
            }
        }

        $data = array();
        foreach($this->_fields as $meta) {
            $field = $meta->name;
            if(isset($this->$field)) {
                $data[$field]   = ($meta->type == 'int' && $this->$field === '') ? NULL : $this->$field;
            }
        }
        if(empty($data)) {
            return FALSE;
        }

        $insert = (empty($this->id)) ? TRUE : FALSE;
        if($insert) {
            $id = $this->db->insert($this->_source, $data);
        } else {
            $this->db->where('id', $this->id);
            $id = $this->db->update($this->_source, $data);
        }

        if(!$id) {
            return FALSE;
        }

        $pk = (empty($this->id)) ? $this->db->insert_id() :$this->id;
        $this->first($pk);

        if($insert) {
            if (method_exists($this, "_afterCreate")) {
                if ($this->_afterCreate() == 'cancel') {
                    return FALSE;
                }
            }
        } else {
            if (method_exists($this, "_afterUpdate")) {
                if ($this->_afterCreate() == 'cancel') {
                    return FALSE;
                }
            }
        }

        if (method_exists($this, "_afterSave")) {
            if ($this->_afterSave() == 'cancel') {
                return FALSE;
            }
        }

        return TRUE;
    }

    /**
     * Method to update all attributes according to the conditions provided
     * $obj->update_all("status='A', date='2005-02-02'", "conditions: id > 100");
     * $obj->update_all("status='A', date='2005-02-02'", "conditions: client_id > 100");
     * @return boolean
     */
    public function update_all($values) {

        if (func_num_args() > 1) {
            $params = self::getParams(func_get_args());
        } else {
            return FALSE;
        }

        if (!isset($params['conditions']) || !$params['conditions']) {
            if (isset($params[1])) {
                $params['conditions'] = $params[1];
            } else {
                $params['conditions'] = '';
            }
        }

        $conditions = $this->convertParams(array('conditions'=>$params['conditions']), TRUE);
        $sql        = "UPDATE $this->_source SET $values $conditions";
        return $this->sql($sql);

    }

    /**
     * Converts the parameters of a function or method of parameter by name to an array
     *
     * @param array $params
     * @return array
     */
    public static function getParams($params) {
        $data = array();
        foreach ($params as $p) {
            if (is_string($p)) {
                $match = explode(': ', $p, 2);
                if (isset($match[1])) {
                    $data[$match[0]] = $match[1];
                } else {
                    $data[] = $p;
                }
            } else {
                $data[] = $p;
            }
        }
        return $data;
    }

    /**
     *
     */
    public function convertParams($what, $toSring=FALSE) {
        $select = '';
        if (isset($what['columns'])) {
            if(!$toSring) {
                if(isset($what['escape'])) {
                    $this->db->select($what['columns'], FALSE);
                } else {
                    $this->db->select($what['columns']);
                }
            }
        }
        if (isset($what['join'])) {
            if(!$toSring) {
                $this->db->ar_join[] = $what['join'];
            }
            $select.= " {$what['join']}";
        }
        if(!isset($what['conditions'])) {
            if (isset($what[0])) {
                if (is_numeric($what[0])) {
                    $what['conditions'] =  'id = '.$what[0];
                } else if(strlen($what[0])) {
                    $what['conditions'] = $what[0];
                }
            }
        }
        if (isset($what['conditions'])) {
            if(!$toSring) {
                $this->db->where($what['conditions']);
            }
            $select.= " WHERE {$what['conditions']}";
        }
        if (isset($what['group'])) {
            if(!$toSring) {
                $this->db->group_by($what['group']);
            }
            $select.= " GROUP BY {$what['group']}";
        }
        if (isset($what['having'])) {
            if(!$toSring) {
                $this->db->having($what['having']);
            }
            $select.= " HAVING {$what['having']}";
        }
        if (isset($what['order'])) {
            if(!$toSring) {
                $this->db->order_by($what['order']);
            }
            $select.= " ORDER BY {$what['order']}";
        }
        if (isset($what['limit']) && isset($what['offset'])) {
            if(!$toSring) {
                $this->db->limit($what['limit'], $what['offset']);
            }
            $select.= " LIMIT {$what['offset']}, {$what['limit']} ";

        } else if (isset($what['limit'])) {
            if(!$toSring) {
                $this->db->limit($what['limit']);
            }
            $select.= " LIMIT {$what['limit']} ";
        }

        return $select;
    }

    /**
     * Paginate
     */
    public function paginate() {

        $params         = self::getParams(func_get_args());

        if(!defined('PER_PAGE')) {
            define('PER_PAGE', 15);
        }

        $page_number    = empty($params['page']) ? 1 : $params['page'];
        $per_page       = empty($params['per_page']) ? PER_PAGE : $params['per_page'];
        $counter        = ($page_number > 1) ? ( ($page_number * $per_page) - ($per_page-1) ) : 1;
        $start          = $per_page * ($page_number - 1);

        $page           = new stdClass();

        $find_args      = array(); //Arreglo que contiene los argumentos para el find
        $conditions     = null;

        if (isset($params['conditions'])) {
            $conditions = "conditions: {$params['conditions']}";
        } else if (isset($params[1])) {
            $conditions = "conditions: {$params[1]}";
        }

        if (isset($params['columns'])) {
            $find_args[]= "columns: {$params['columns']}";
        }
        if (isset($params['join'])) {
            $find_args[]= "join: {$params['join']}";
        }
        if (isset($params['group'])) {
            $find_args[]= "group: {$params['group']}";
        }
        if (isset($params['having'])) {
            $find_args[]= "having: {$params['having']}";
        }
        if (isset($params['order'])) {
            $find_args[]= "order: {$params['order']}";
        }
        if (isset($params['distinct'])) {
            $find_args[]= "distinct: {$params['distinct']}";
        }

        //Count by paginated
        $find_args[]    = "paginated: ".true;

        if (isset($conditions)) {
            $find_args[]= $conditions;
        }

        $total_items    = call_user_func_array(array($this , 'count'), $find_args);

        $find_args[]    = "offset: $start";
        $find_args[]    = "limit: $per_page";
        $page->items    = call_user_func_array(array($this , 'find'), $find_args);

        $page->next     = ($start + $per_page) < $total_items ? ($page_number + 1) : false;
        $page->prev     = ($page_number > 1) ? ($page_number - 1) : false;
        $page->current  = $page_number;
        $page->total_page = ceil($total_items / $per_page);
        if( ($page->total_page < $page_number) && ($total_items > 0)){
            $page->prev = false;
        }
        $page->counter  = ($total_items >= $counter) ? $counter : 1;
        $page->size     = $total_items;
        $page->per_page = $per_page;

        return $page;
    }

    /**
     * Deletes data from Relational Map Table
     *
     * @param mixed $what
     */
    public function delete($what = '') {
        if (func_num_args() > 1) {
            $what = self::getParams(func_get_args());
        }

        $conditions = '';
        if (is_array($what)) {
            if ($what["conditions"]) {
                $conditions = $what["conditions"];
            }
        } else {
            if (is_numeric($what)) {
                $conditions = "id = '$what'";
            } else if(isset($this->id) && empty($what)) {
                $conditions = "id = $this->id";
            } else {
                $conditions = $what;
            }
        }

        if (method_exists($this, "_beforeDelete")) {
            if ($this->_beforeDelete() == 'cancel') {
                return false;
            }
        }

        $val = $this->db->delete($this->_source, $conditions);

        if ($val) {
            if (method_exists($this, "_afteDelete")) {
                if ($this->_afterDelete() == 'cancel') {
                    return false;
                }
            }
        }

        return $val;
    }

    /**
     * SQL
     */
    public function sql($sql) {
        $rs = $this->db->query($sql);
        if(is_object($rs)) {
            return $rs->result();
        }
        return $rs;
    }

}