<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Proyecto Final Redes 3 - 2015</title>
        <link rel="stylesheet" href="//<?= $_SERVER['HTTP_HOST']; ?>/theme/stylesheets/cms.css">
        <script src="//<?= $_SERVER['HTTP_HOST']; ?>/theme/bower_components/jquery/dist/jquery.min.js"></script>
    </head>

    <body class="login">

        <section class="login-wrapper">
            <div class="small-12 medium-10 large-6 columns small-centered">

                <div class="logo">
                    <h3>Proyecto Final - 2015</h3>
                </div>

                <div id="flash-message" class="flash-message medium-12">
                    <?php Flash::output(); ?>
                </div>

                <div class="login-box">
                    <form id="form-login" method="post" action="//<?= $_SERVER['HTTP_HOST']; ?>/cmsadmin/sessions/login" data-abide >
                        <div class="row">
                            <div class="medium-12 columns">
                                <h3>Inicia sesión</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row medium-8">
                            <div class="medium-12 columns">
                                <?php echo form_label('Nombre de usuario', 'login', array('class' => 'to-placeholder')); ?>
                                <?php echo form_input('login', NULL, 'placeholder="Nombre de usuario" required'); ?>
                                <small class="error"><i class="fa fa-warning"></i> Este campo es requerido</small>
                            </div>
                        </div>
                        <div class="row medium-8">
                            <div class="medium-12 columns">
                                <?php echo form_label('Contraseña', 'password', array('class' => 'to-placeholder')); ?>
                                <?php echo form_password('password', NULL, 'placeholder="Contraseña" required'); ?>
                                <small class="error"><i class="fa fa-warning"></i> Este campo es requerido</small>
                            </div>
                        </div>
                        <hr>
                        <div class="row medium-8">
                            <div class="medium-12 columns text-center">
                                <?php echo form_submit('submit', 'Submit', 'class="success button round small button-login" id="btn-login"'); ?>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="center text-center">
                    <p>UNIVERSIDAD DISTRITAL FRANCISCO JOSÉ DE CALDAS
                        FACULTAD DE INGENIERÍA
                        PROYECTO CURRICULAR DE INGENIERÍA DE SISTEMAS
                        REDES DE COMUNICACIONES III
                        PROYECTO FINAL</p>
                </div>
            </div>
        </section>

        <script src="//<?= $_SERVER['HTTP_HOST']; ?>/theme/bower_components/foundation/js/foundation.min.js"></script>
        <script src="//<?= $_SERVER['HTTP_HOST']; ?>/theme/bower_components/foundation/js/vendor/modernizr.js"></script>
        <script src="//<?= $_SERVER['HTTP_HOST']; ?>/theme/js/foundation-datepicker.js"></script>
        <script src="//<?= $_SERVER['HTTP_HOST']; ?>/theme/js/utility.js"></script>
        <script src="//<?= $_SERVER['HTTP_HOST']; ?>/theme/js/cms.js"></script>

    </body>
</html>
