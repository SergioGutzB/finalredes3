<?php

$view = (empty($view)) ? 'dashboard' : $view;

if($view == 'sessions/login'){
    $this->load->view('cmsadmin/sessions/login');
} else{
    if($this->input->is_ajax_request()) {
        $this->load->view("cmsadmin/$view");
    } else {
        $this->load->view('partials/header');
        $this->load->view("cmsadmin/$view");
        $this->load->view('partials/footer');
    }
}