<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/admin_controller.php';

class Sessions extends Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * Index
     */
    public function index() {
        redirect('cmsadmin/sessions/login/');
    }

    /**
     * Login
     */
    public function login($ajax = '') {

        if ($ajax) {
            return $this->load->view('partials/session');
        }
        if ($this->session->userdata('login')) {
            redirect('cmsadmin/dashboard/');
        } else {
            if ($this->input->post('login')) {

                $this->form_validation->set_rules('login', 'Username', 'required|min_length[5]');
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

                if ($this->form_validation->run() === true) {

                    $login = filter_var($this->input->post('login'), FILTER_SANITIZE_STRING);
                    $pass = filter_var(sha1($this->input->post('password')), FILTER_SANITIZE_STRING);

                    if (empty($login) OR empty($pass)) {
                        Flash::error('Your Username or Password is incorrect. Try again');
                    } else {

                        $user = new User_model();
                        $count = $user->count("login = '$login' AND pass = '$pass' AND active = 1");
                        if ($count > 0) {
                            $user = $user->first("login = '$login' AND pass = '$pass' AND active = 1");
                            $this->session->set_userdata($user);
                            redirect('cmsadmin/dashboard/');
                        } else {
                            Flash::error('Your Username or Password is incorrect. Try again');
                        }
                    }
                } else {
                    Flash::error(validation_errors());
                }
            }
            $data['view'] = 'sessions/login';
            $this->load->view('/partials/base', $data);
        }
    }

    public function logout() {
        Flash::clean();
        $this->session->sess_destroy();
        redirect('cmsadmin/sessions/login/');
    }

}
