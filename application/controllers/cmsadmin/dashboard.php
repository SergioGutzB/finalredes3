<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/admin_controller.php';

class Dashboard extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Api');
        if (isset($_POST)) {
            $this->data_input = file_get_contents("php://input");
        }
    }

    /**
     * Main Method
     */
    public function index($filtro = 1) {
        $data['view'] = 'dashboard';
        $data_post = json_decode($this->data_input);
        $this->load->view(THEME_VIEWS . '/partials/base', $data);
    }

    public function mail (){
        $data['view'] = 'mail';
        $data_post = json_decode($this->data_input);
        $this->load->view(THEME_VIEWS . '/partials/base', $data);
    }

      public function ftp (){
        $data['view'] = 'ftp';
        $data_post = json_decode($this->data_input);
        $this->load->view(THEME_VIEWS . '/partials/base', $data);
    }


}
