<?php

class Utils {

    /*
     * Method to get ip from a client
     */
    public static function getIp() {
        return empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }


    /**
     * Generate slug from a text
     */
    public static function getSlug($string, $separator = '-', $length = 100) {
        $search = explode(',', 'ç,Ç,ñ,Ñ,æ,Æ,œ,á,Á,é,É,í,Í,ó,Ó,ú,Ú,à,À,è,È,ì,Ì,ò,Ò,ù,Ù,ä,ë,ï,Ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,Š,Œ,Ž,š,¥');
        $replace = explode(',', 'c,C,n,N,ae,AE,oe,a,A,e,E,i,I,o,O,u,U,a,A,e,E,i,I,o,O,u,U,ae,e,i,I,oe,ue,y,a,e,i,o,u,a,e,i,o,u,s,o,z,s,Y');
        $string = str_replace($search, $replace, $string);
        $string = strtolower($string);
        $string = preg_replace('/[^a-z0-9_]/i', $separator, $string);
        $string = preg_replace('/\\' . $separator . '[\\' . $separator . ']*/', $separator, $string);
        if (strlen($string) > $length) {
            $string = substr($string, 0, $length);
        }
        $string = preg_replace('/\\' . $separator . '$/', '', $string);
        $string = preg_replace('/^\\' . $separator . '/', '', $string);
        return $string;
    }

    /***
     * Trucate words
     */
    public static function truncateWords($text, $limit, $end='...') {
        $text   =   strip_tags($text);
        if (strlen($text) > $limit) {
            $words  = str_word_count($text, 2);
            $pos    = array_keys($words);
            $aux    = trim(@substr($text, 0, $pos[$limit])).$end;
            if($aux != $end) {
                $text = $aux;
            }
        }
        return $text;
    }

}

?>
