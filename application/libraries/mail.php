<?php

/**
 * Cargador para la libreria PHPMailer
 *
 **/
require_once APPPATH.'libraries/phpmailer/class.smtp.php';
require_once APPPATH.'libraries/phpmailer/class.phpmailer.php';

class Mail extends PHPMailer {

    /**
     * CharSet del mensaje
     * @var string
     */
    public $CharSet = 'UTF-8';

    /**
     * Tipo de contenido
     * @var string
     */
    public $ContentType = 'text/html';

    /**
     * Método constructor
     * @param string $name Nombre de quien envía el mensaje
     * @param string $from Email de quien envía el mensaje
     * @param string $pass Contraseña del usuario
     * @param string $host Nombre del host
     * @param numeric $port Número del puerto
     * @param string $secure Tipo de cifrado
     */
    public function __construct($name='Amtrak Train Days', $from='info@amtraktraindays.com', $exceptions = false) {
        $this->From     = $from;
        $this->FromName = $name;
        $this->SetLanguage('en', APPPATH.'libraries/phpmailer/language/');
        $this->PluginDir = APPPATH.'libraries/phpmailer/';
        $this->SMTPDebug = 0;
        parent::__construct($exceptions);
        $this->SMTPConfig('sethdebrain', 'hd3br41n14', 'smtp.sendgrid.net', 465, 'ssl');
    }

    /**
     * Método para signar el motivo del email
     * @param string $text
     */
    public function Subject($text) {
        $this->Subject = $text;
    }

    /**
     * Método para crear el cuerpo
     * @param string
     */
    public function BodyHtml($partial, $params=array(), $theme='') {
        ob_start();
        $ci =& get_instance();
        $ci->load->view("$theme/partials/mail/$partial", $params);
        $this->Body = ob_get_clean();
    }

    /**
     * Método para definir el SMTP
     * @param string $user Ejemplo: nombre@dominio.ext
     * @param string $pass Ejemplo: 123456
     * @param string $host Ejemplo: smtp.gmail.com
     * @param numeric $port Ejemplo: 465
     * @param string $secure Ejemplo: SSL ó TTL
     */
    public function SMTPConfig($user, $pass, $host, $port, $secure='') {
        $this->Username = $user;
        $this->Password = $pass;
        $this->Host     = $host;
        $this->Port     = $port;
        if(!empty($secure)) {
            $this->SMTPSecure = $secure;
        }
        $this->SMTPAuth = TRUE;
        parent::IsSMTP();
    }

    /**
     * Método para enviar el mensaje
     * @return boolean
     */
    public function SendMail() {
        $send = $this->Send();
        if(!$send) {
            $send = $this->Send();
        }
        return $send;
    }

    /**
     * Método para obtener el error del mensaje
     * @return type
     */
    public function GetError() {
        return $this->ErrorInfo;
    }

}


