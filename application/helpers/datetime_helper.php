<?php

function show_date($date, $format="m/d/Y") {
    return date($format, strtotime($date));
}

function convert_date($date, $format="Y-m-d") {
    return show_date($date, $format);
}

function show_time($time, $format="h:i A") {
    return date($format, strtotime($time));
}

function convert_time($time, $format="H:i:s") {
    return show_time($time, $format);
}

function set_timezone($utc) {
    $zonelist = array('Kwajalein' => -12.00, 'Pacific/Midway' => -11.00, 'Pacific/Honolulu' => -10.00, 'America/Anchorage' => -9.00, 'America/Los_Angeles' => -8.00, 'America/Denver' => -7.00, 'America/Tegucigalpa' => -6.00, 'America/New_York' => -5.00, 'America/Caracas' => -4.30, 'America/Halifax' => -4.00, 'America/St_Johns' => -3.30, 'America/Argentina/Buenos_Aires' => -3.00, 'America/Sao_Paulo' => -3.00, 'Atlantic/South_Georgia' => -2.00, 'Atlantic/Azores' => -1.00, 'Europe/Dublin' => 0, 'Europe/Belgrade' => 1.00, 'Europe/Minsk' => 2.00, 'Asia/Kuwait' => 3.00, 'Asia/Tehran' => 3.30, 'Asia/Muscat' => 4.00, 'Asia/Yekaterinburg' => 5.00, 'Asia/Kolkata' => 5.30, 'Asia/Katmandu' => 5.45, 'Asia/Dhaka' => 6.00, 'Asia/Rangoon' => 6.30, 'Asia/Krasnoyarsk' => 7.00, 'Asia/Brunei' => 8.00, 'Asia/Seoul' => 9.00, 'Australia/Darwin' => 9.30, 'Australia/Canberra' => 10.00, 'Asia/Magadan' => 11.00, 'Pacific/Fiji' => 12.00, 'Pacific/Tongatapu' => 13.00);
    $utc        = trim(preg_replace('/[A-Z]/', '', $utc)).'.00';
    $time_zone  = array_search($utc, $zonelist);
    if($time_zone) {
        date_default_timezone_set($time_zone);
    }
}

function get_day_name($d) {
    $items = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Monday');
    return $items[$d];
}

function get_date($date, $format="d") {
    $time       = strtotime($date);
    return      date($format, $time);
}

function get_range_week() {
    $today      = date("D");
    $monday     = ($today == 'Mon') ? date("Y-m-d") : date("Y-m-d", strtotime("last Monday"));
    $sunday     = date('Y-m-d', strtotime($monday. ' + 6 days'));
    return array('monday'=>$monday, 'sunday'=>$sunday);
}

function get_day_event($start_date) {
    //07 JAN, 5:00 PM
    $time       = strtotime($start_date);
    $text       = " ".date("d", $time);
    $text      .= " ".strtoupper(date("M", $time));
    $text      .= ", ".show_time($start_date);
    return $text;
}