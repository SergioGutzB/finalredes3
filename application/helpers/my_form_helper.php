 <?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Common Input Field
 *
 * @access    public
 * @param    string
 * @param    mixed
 * @param    string
 * @param    string
 * @return    string
 */
if ( ! function_exists('form_common'))
{
    function form_common($type = 'text', $data = '', $value = '', $extra = '')
    {
        $defaults = array('type' => $type, 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

        return "<input "._parse_form_attributes($data, $defaults).$extra." />";
    }
}

/**
 * Email Input Field
 *
 * @access    public
 * @param    mixed
 * @param    string
 * @param    string
 * @return    string
 */
if ( ! function_exists('form_email'))
{
    function form_email($data = '', $value = '', $extra = '')
    {
        return form_common($type = 'email', $data, $value, $extra);
    }
}

/**
 * Url Input Field
 *
 * @access    public
 * @param    mixed
 * @param    string
 * @param    string
 * @return    string
 */
if ( ! function_exists('form_url'))
{
    function form_url($data = '', $value = '', $extra = '')
    {
        return form_common($type = 'url', $data, $value, $extra);
    }
}

/**
 * Number Input Field
 *
 * @access    public
 * @param    mixed
 * @param    string
 * @param    string
 * @return    string
 */
if ( ! function_exists('form_number'))
{
    function form_number($data = '', $value = '', $extra = '')
    {
        return form_common($type = 'number', $data, $value, $extra);
    }
}

/**
 * Number Input Field
 *
 * @access    public
 * @param    mixed
 * @param    string
 * @param    string
 * @return    string
 */
if ( ! function_exists('form_range'))
{
    function form_range($data = '', $value = '', $extra = '')
    {
        return form_common($type = 'range', $data, $value, $extra);
    }
}


if ( ! function_exists('form_dbselect'))
{
    /**
     *
     * @param string $name
     * @param string $show
     * @param array $data
     * @param int $selected
     * @param string $blank
     * @param string $extra
     * @return string
     */
    function form_dbselect($name, $show, $options, $selected = '', $blank = '', $extra = '') {
        // If no selected state was submitted we will attempt to set it automatically
        if (empty($selected)) {
            // If the form name appears in the $_POST array we have a winner!
            if (isset($_POST[$name])) {
                $selected = array($_POST[$name]);
            }
        }

        if ($extra != '')
            $extra = ' ' . $extra;

        $multiple = (is_array($selected) && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select name="' . $name . '"' . $extra . $multiple . ">\n";

        if (!empty($blank)) {
            $form .= '<option value="">' . $blank . '</option>';
        }

        foreach ($options as $option) {
            if(is_array($selected)) {
                $sel    = (in_array($option->id, $selected)) ? ' selected="selected"' : '';
            } else {
                $sel    = ($option->id == $selected) ? ' selected="selected"' : '';
            }
            $form  .= '<option value="' . $option->id . '"' . $sel . '>' . $option->$show . "</option>\n";
        }

        $form .= '</select>';

        return $form;
    }

}


/* End of file MY_form_helper.php */
/* Location: ./application/helpers/MY_form_helper.php */