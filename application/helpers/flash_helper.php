<?php
/**   
 * 
 * Flash::info('hola mundo');
 * Flash::output(); //return echo "hola mundo";
 * 
 */

class Flash {
       
    /**
     * Mensajes almacenados en un request
     */
    private static $_contentMsj = array();
        
    /**
     * Setea un mensaje
     *
     * @param string $name Tipo de mensaje y para CSS class='$name'.
     * @param string $msg Mensaje a mostrar
     * @param boolean $audit Indica si el mensaje se almacena como auditoría
     */
    public static function set($name, $msg) { 
        $CI =& get_instance();                
        //Verifico si hay mensajes almacenados en sesión por otro request.
        if(self::hasMessage()) {            
            self::$_contentMsj = $CI->session->userdata('flash_message');
        }        
        $tmp_id              = mt_rand(100, 10000);
        self::$_contentMsj[] = '<div id="alert-id-'.$tmp_id.'" data-alert class="alert-box radius '.$name.'">'.$msg.'<a href="#" class="close">&times;</a></div>'.PHP_EOL.'<script type="text/javascript">$("#alert-id-'.$tmp_id.'").hide().fadeIn(500).delay(10000).fadeOut(500);</script>';

        //Almaceno los mensajes guardados en una variable de sesión, para mostrar los mensajes provenientes de otro request.
        $CI->session->set_userdata('flash_message', self::$_contentMsj);        
    }
    
    /**
     * Verifica si tiene mensajes para mostrar.
     *
     * @return bool
     */
    public static function hasMessage() {
        $CI =& get_instance();        
        return ($CI->session->userdata('flash_message')) ? TRUE : FALSE;        
    }
    
    /**
     * Método para limpiar los mensajes almacenados
     */
    public static function clean() {
        //Reinicio la variable de los mensajes
        self::$_contentMsj = array();
        //Elimino los almacenados en sesión
        $CI =& get_instance();        
        $CI->session->set_userdata('flash_message', array());        
    }

    /**
     * Muestra los mensajes
     */
    public static function output() {
        if(self::hasMessage()) {
            $CI =& get_instance();        
            //Asigno los mensajes almacenados en sesión en una variable temporal
            $tmp = $CI->session->userdata('flash_message');            
            //Recorro los mensajes
            foreach($tmp as $msg) {
                // Imprimo los mensajes
                echo $msg;
            }
            self::clean();
        }
    }
    
    /**
     * Retorna los mensajes cargados como string
     */
    public static function toString() {
        $CI =& get_instance();                
        //Asigno los mensajes almacenados en sesión en una variable temporal
        $tmp = self::hasMessage() ? $CI->session->userdata('flash_message') : array();
        $msg = array();
        //Recorro los mensajes
        foreach($tmp as $item) {
            //Limpio los mensajes
            $item  = explode('<script', $item);
            if(!empty($item[0])) {
                $msg[] = str_replace('&times;', '', strip_tags($item[0]));                
            }
        }
        $flash = trim(strip_tags(ob_get_clean())); //Almaceno los mensajes que hay en el buffer por el Flash::
        $msg = trim(join('<br />', $msg));
        self::clean(); //Limpio los mensajes de la sesión               
        return ($flash) ? $flash.'<br />'.$msg : $msg;
    }

    /**
     * Carga un mensaje de error
     *
     * @param string $msg
     */
    public static function error($msg) {
        self::set('alert',$msg);          
    }

    /**
     * Carga un mensaje de advertencia en pantalla
     *
     * @param string $msg
     */
    public static function warning($msg) {
        self::set('warning',$msg);
    }

    /**
     * Carga informacion en pantalla
     *
     * @param string $msg
     */
    public static function info($msg) {
        self::set('info',$msg);
    }
    
    /**
     * Carga información de suceso correcto en pantalla
     *
     * @param string $msg
     */
    public static function valid($msg) {
        self::set('success',$msg);
    } 
    
}
