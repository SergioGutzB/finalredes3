// Foundation JavaScript
$(document).foundation({abide: {live_onblur: false}});


$(function () {

    /**************
     * CREAR PROYECTO
     // *************/
    $('body').on('click', '#add-project', function (e) {
        e.preventDefault();
        openPanel('panel-dashboard ');
    });

    $('#save_project').click(function (e) {
        e.preventDefault();
        var data = $('#form-project').serializeObject();
        var id = "";
        $.post('/cmsadmin/proyecto/guardar', $.toJSON(data), function (r) {
            if (parseInt(r) != null)
                id = r;
        }, 'json').always(function () {
            $('#panel-dashboard').foundation('reveal', 'close');
            if (id != 0)
                window.location.href = ("/cmsadmin/proyecto/view/" + id);
            else  window.location.reload();
        });
    });

    $('#update_project').click(function (e) {
        e.preventDefault();
        var data = $('#form-project-u').serializeObject();
        $.post('/cmsadmin/proyecto/guardar', $.toJSON(data), function (r) {
            if (r == 'success') {
            } else {
                alert('There was an unexpected error. Please try again later');
            }
        }, 'json').always(function () {
            $('#panel-project').foundation('reveal', 'close');
            window.location.href = ("/");
        });
    });


    /**************
     * SERVIDORES
     *************/
    $('.create-server').click(function (e) {
        e.preventDefault();
        var data = $('#form-add-server').serializeObject();
        $.post('/cmsadmin/servidor/guardar', $.toJSON(data), function (r) {
            if (r == 'success') {
                alert('success');
            } else {
                alert('There was an unexpected error. Please try again later');
            }
        }, 'json').always(function () {
            $('#addServerModal').foundation('reveal', 'close');
            window.location.reload();

        });

    });
    /**************
     * EDIT SERVER
     ***************/
    $(".edit-server").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        console.log(id);
        $.ajax({
            data: {'id': id},
            url: "/cmsadmin/servidor/view_json/" + id,
            type: 'get',
            success: function (data) {
                console.log(data);
                $('#addServerModal').foundation('reveal', 'open');
                $("#id-server").attr("value", id);
                $("#nombre_servidor").attr("value", data[0].nombre_servidor);
                $("#nombre_usuario").attr("value", data[0].nombre_usuario);
                $("#ip").attr("value", data[0].ip);
                $("#password").attr("value", data[0].password);
                $("#puerto").attr("value", data[0].puerto);
                $("#llave").attr("value", data[0].llave);
                $("#informacion").attr("value", data[0].informacion);
            }
        });
    });

    $(".deleted-server").click(function () {
        var id = $(this).data("id");

        $.post('/cmsadmin/servidor/delet/' + id, function (r) {
            if (r == 'success') {
                console.log("eliminando server");
            } else {
            }
        }).always(function () {
            window.location.reload();
        });
    });
    /**************
     * CREATE AND UPDATE  USER
     ***************/
    $('.create-user').click(function (e) {
        e.preventDefault();
        var form = $(this).data("form");
        var data = $('#' + form + '').serializeObject();
        console.log(form);
        console.log(data);
        $.post('/cmsadmin/user/guardar', $.toJSON(data), function (r) {
            if (r == 'success') {
            } else {
            }
        }, 'json').always(function () {
            $('#addUserModal').foundation('reveal', 'close');
            window.location.href = ("/users/");
        });
    });
    $('.eliminar_usuario').click(function (e) {
        alert("Si el usuario tiene un proyecto a cargo, el proyecto también sera eliminado");
    });
    /**************
     * EDIT USER
     ***************/
    $(".edit-user").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        $.ajax({
            data: {'id': id},
            url: "/cmsadmin/user/ver/" + id,
            type: 'get',
            success: function (data) {

                $("#e-id").attr("value", id);
                $("#e-nombre_usuario").attr("value", data[0].nombre_usuario);
                $("#e-login").attr("value", data[0].login);
                $("#url-user").attr("value", "/files/" + data[0].imagen);
                $("#img-user").attr("src", "/files/" + data[0].imagen);
                 $('#editUserModal').foundation('reveal', 'open');


            }
        });
    });

    /**************
     * DELETED USER
     ***************/
    $('.del-user').click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var nom = $(this).data("nombre");
        var data = {"id": id}
        $.post('/cmsadmin/user/delet', $.toJSON(data), function (r) {
            if (parseInt(r) == -2) {
                $('#modalTitulo').text("Eliminando Usuario");
                $('#modalTexto').text("No se puede eliminar el usaurio " + nom + " porque está asociado a un proyecto");
                $('#alertModal').foundation('reveal', 'open');
            }
            if (parseInt(r) == 1) {
                window.location.href = ("/users/");
            }
        }, 'json');
    });
    /**************
     * EDIT CLIENT
     ***************/
    $(".edit-client").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        console.log(id);
        $.ajax({
            data: {'id': id},
            url: "/cmsadmin/client/view_json/" + id,
            type: 'get',
            success: function (data) {
                console.log(data);
                $('#clientModal').foundation('reveal', 'open');
                $("#e-id").attr("value", id);
                $("#e_nombre_cliente").attr("value", data[0].nombre_cliente);
                $("#e_documento").attr("value", data[0].documento);
                $("#e_dirrecion").attr("value", data[0].dirrecion);
                $("#e_telefono").attr("value", data[0].telefono);
            }
        });
    });
    /**************
     * CREATE CLIENT
     ***************/
    $('.add-client').click(function (e) {
        e.preventDefault();
        var data = $('#form-add-client').serializeObject();
        $.post('/cmsadmin/client/save', $.toJSON(data), function (r) {
            console.log(data);
        }, 'json').always(function () {
            $('#alertModal').foundation('reveal', 'close');
            window.location.href = ("/clients/");
        });
    });

    /**************
     * DELETE CLIENT
     ***************/
    $('.del-client').click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var nom = $(this).data("nombre");
        var data = {"id": id}
        $.post('/cmsadmin/client/delet', $.toJSON(data), function (r) {
            if (parseInt(r) == -2) {
                $('#modalTitulo').text("Eliminando Cliente ");
                $('#modalTexto').text("No se puede eliminar el cliente " + nom + " porque está asociado a un proyecto");
                $('#delClientModal').foundation('reveal', 'open');
            }
            if (parseInt(r) == 1) {
                window.location.href = ("/clients/");
            }
        }, 'json');
    });

    /**************
     * FILTRAR PROYECTOS
     ***************/
    $("#filtro").change(function (e) {
        e.preventDefault();
        var filtro = $(this).val();
        console.log($(this).val());
        window.location.href = ("/cmsadmin/dashboard/index/" + filtro);

    })

    $('#view_project').click(function () {
        var id = $(this).data("id");
        listar(id);
    });

    /**************
     * CHECKED LIST QA
     ***************/

    $(".ch").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var user = $(this).data("user");
        var d = new Date();
        console.log(id);
        var id_project = $(this).data("project");
        var fecha_completada = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        if (!$(this).attr("checked")) {
            var data = {
                "id": id,
                "completada": 1,
                "usuario": user,
                "fecha_completada": fecha_completada,
            };
            $.post('/cmsadmin/proyecto/checked', $.toJSON(data), function (r) {
            }, 'json').always(function () {
                window.location.reload();
            });
        } else {
            var data = {
                "id": id,
                "completada": 0,
                "usuario": user,
                "fecha_completada": "",
            };
            $.post('/cmsadmin/proyecto/checked', $.toJSON(data), function (r) {
            }, 'json').always(function () {
                window.location.reload();
            });
        }
    });
    /**************
     *CREAR QAs
     ***************/
    $("#create-qa").click(function (e) {
        e.preventDefault();
        var data = $('#form-add-qa').serializeObject();
        console.log(data);
        console.log("creando regla");
        $.post('/cmsadmin/proyecto/create_qa', $.toJSON(data), function (r) {
        }, 'json').always(function () {
            $('#addQuality').foundation('reveal', 'close');
            $(document).foundation('tab', 'reflow');
            window.location.reload();
        });
    });
    /**************
     * EDIT CONTAC
     ***************/

    $(".edit-contact").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        console.log(id);
        $.ajax({
            data: {'id': id},
            url: "/cmsadmin/contact/view/" + id,
            type: 'get',
            success: function (data) {
                console.log(data);
                $('#addContactModal').foundation('reveal', 'open');
                $("#id_contact").attr("value", id);
                $("#nombre_contacto").attr("value", data[0].nombre_contacto);
                $("#email_contacto").attr("value", data[0].email_contacto);
                $("#telefono").attr("value", data[0].telefono);
            }
        });
    });
    /**************
     * CREATE CONTAC
     ***************/
    $('.create-contact').click(function (e) {
        e.preventDefault();
        var data = $('#form-add-contact').serializeObject();
        $.post('/cmsadmin/contact/save', $.toJSON(data), function (r) {
            if (r == 'success') {
                alert('success');
            } else {
                alert('There was an unexpected error. Please try again later');
            }
        }, 'json').always(function () {
            $('#addContactModal').foundation('reveal', 'close');
            window.location.reload();
        });
    });
    /**************
     * DELETED CONTAC
     ***************/
    $(".del-contact").click(function () {
        var id = $(this).data("id");
        var nom = $(this).data("nombre");
        var data = {"id": id};
        $.post('/cmsadmin/contact/delet/', $.toJSON(data), function (r) {
            if (parseInt(r) == -2) {
                $('#modalTitulo').text("Eliminando Contacto ");
                $('#modalTexto').text("No se puede eliminar el contacto " + nom + " porque está asociado a un cliente");
                $('#alertModal').foundation('reveal', 'open');
                console.log("No se puede eliminar el contacto porque está asociado a un cliente");
            }
            if (parseInt(r) == 1) {
                window.location.href = ("/contacts/");
            }
        }, 'json');
    });
    /**************
     * VIEW CONTAC
     ***************/
    $(".view_contact").click(function () {
        var email = $(this).data("email");
        var telefono = $(this).data("tel");
        var nombre = $(this).data("nombre");
        $('#modalTitle').text(nombre);
        $('#contactModal-email').attr("value", email);
        $('#contactModal-telefono').attr("value", telefono);
        $('#viewContactModal').foundation('reveal', 'open');
    });


    $("input[type=text]").focus(function () {
        this.select();
    });

});
