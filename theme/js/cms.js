/*****
 * MAIN
 */
$('body').on('click', '#main-menu a', function(e) {
    e.preventDefault();
    var este            = $(this);
    var container       = este.parents('ul:first');
    var items           = container.find('li');
    items.removeClass('active');
    este.parents('li:first').addClass('active');
    return true;
});

function bindMain() {
    var container   = $('#main-menu');
    var tmp_path    = window.location.pathname.split('/');
    var path        = '/'+tmp_path[0];
    if(tmp_path[1] != undefined) {
        path        = path+tmp_path[1]+'/';
    }
    if(tmp_path[2] != undefined) {
        path        = path+tmp_path[2]+'/';
    }
    var item        = container.find('a[href="'+path+'"]');
    var items       = container.find('li');
    items.removeClass('active');
    item.parents('li:first').addClass('active');
}

//Input uniqe
$('body').on('blur', '.input-unique', function(e) {
    e.preventDefault();
    var este    = $(this);
    var field   = este.attr('data-unique');
    var form    = este.parents('form:first');
    var id      = form.find('input[name="event[id]"]');
    id          = (id.size() > 0) ? id.val() : '';
    if (este.val().length === 0 && este.attr('required') !== undefined) {
        showInputError(este);
    } else {
        var url = '/cmsadmin/events/check/'+field+'/';
        var data = {
            value: este.val(),
            alias: (este.attr('data-alias') !== undefined) ? este.attr('data-alias') : '',
            id: id
        };
        $.post(url, $.toJSON(data), function(r) {
            if(r.error !== false) {
                var msg = (r.msg !== undefined) ? r.msg : 'Oops! An internal server error has occurred.\nPlease try again';
                flashError(msg);
                showInputError(este, 1000);
                form.find('[type="submit"]').attr('disabled', 'disabled');
            } else {
                removeInputError(este);
                form.find('[type="submit"]').removeAttr('disabled');
            }
        }, 'json');
    }

});

/*********
 * TABS
 ********/
$(function() {
    $('body').on('click', '#event-tabs dd', function (event, tab) {
        $(document).foundation('orbit', 'reflow');
        //var form = tab.parents('form:first');
        //form.find('.input-required').removeAttr('required');
        //tab.find('.input-required').attr('required', '');
    });
});

/**********
 * EVENTS
 **********/
//Search
$('body').on('submit', '#form-event-search', function(e) {
    e.preventDefault();
    var data    = $(this).find('input[name="event"]').val();
    var url     = '/cmsadmin/dashboard/search/'+data+'/';
    $.remote({url: url, change_url: true, capa: 'view-content'});
});

//Orbit
$(function() {
    $('body').on('click', '.images-orbit li', function() {
        var container = $(this).parents('.images-container:first');
        container.find('input[type="hidden"]:first').val($(this).find('img').attr('srco'));
        container.find('li').removeAttr('data-selected');
        $(this).attr('data-selected', '');
    });
});

//Callback before send
function getGeolocate() {
    var este    = $('#form-event');
    var address = este.find('input[name="event[address]"]').val();
    var city    = este.find('input[name="event[city]"]').val();
    var zip     = este.find('input[name="event[zip]"]').val();
    var state_id= este.find('select[name="event[state_id]"]').val();
    $.ajaxSetup({async: false});
    $.get('/cmsadmin/events/get_state/'+state_id, function(r){
        $.get('http://maps.googleapis.com/maps/api/geocode/json?address='+address+',+'+city+',+'+r.state.name+',+USA', function(r) {
            este.find('input[name="event[latitude]"]').val(r.results[0].geometry.location.lat);
            este.find('input[name="event[longitude]"]').val(r.results[0].geometry.location.lng);
        }, 'json');
    }, 'json');
    $.ajaxSetup({async: true});
    return true;
}

//Submit
$('body').on('submit', '#form-event', function(e) {
    e.preventDefault();
    var este    = $(this);
    var submit  = este.find('input[type="submit"]:visible');
    var tmp_val = submit.attr('name').split('_')[1];
    $('input[name="tab"]').val(tmp_val);
});

//Publish (Open Modal)
$('body').on('click', '.btn-event-pusblish', function(e) {
    e.preventDefault();
    var este = $(this);
    if(este.attr('data-event') != undefined)  {
        $('#modal-event-publish').find('input[name="event"]').val(este.attr('data-event'));
        $('#modal-event-publish').find('input[name="send_email"]').removeAttr('checked');
    }
    $('#modal-event-publish').foundation('reveal', 'open');
});

//Publish Send Confirmation
$('body').on('click', '.btn-event-confirm', function(e) {
    e.preventDefault();
    var modal   = $(this).parents('.reveal-modal').first();
    var email   = modal.find('input[name="send_email"]:checked').val();
    var event   = modal.find('input[name="event"]').val();
    var url     = (email === '1') ? '/cmsadmin/events/status/publish/'+event+'?send_email=true' : '/cmsadmin/events/status/publish/'+event;
    $.remote({url: url, change_url: false});
    $('#modal-event-publish').foundation('reveal', 'close');
});

//Reject (Open Modal)
$('body').on('click', '.btn-event-reject', function(e) {
    e.preventDefault();
    var este = $(this);
    if(este.attr('data-event') != undefined)  {
        $('#modal-event-reject').find('input[name="event"]').val(este.attr('data-event'));
        $('#modal-event-reject').find('input[name="send_email"]').removeAttr('checked');
    }
    $('#modal-event-reject').foundation('reveal', 'open');
});

//Reject Send Confirmation
$('body').on('click', '.btn-event-confirm-reject', function(e) {
    e.preventDefault();
    var modal   = $(this).parents('.reveal-modal').first();
    var email   = modal.find('input[name="send_email"]:checked').val();
    var event   = modal.find('input[name="event"]').val();
    var url     = (email === '1') ? '/cmsadmin/events/status/reject/'+event+'?send_email=true' : '/cmsadmin/events/status/reject/'+event;
    $.remote({url: url, change_url: false});
    $('#modal-event-reject').foundation('reveal', 'close');
});

//Delete
function deleteEvent(este) {
    var url = este.attr('data-to');
    var options = {url: url, change_url: 'cmsadmin/dashboard/'};
    $.remote(options);
}



/**************
 * SPONSORS
 *************/
var items_sponsors  = 0;

function loadSponsors(data) {
    source      = $("#template-sponsors").html();
    template    = Handlebars.compile(source);
    template    = template({sponsors: data});
    $('#list-sponsors').append(template);
    items_sponsors = $("#list-sponsors").find('.item-sponsor').length;
    setTimeout(function() {
        bindFileUpload();
    }, 500);
}

$('body').on('click', '.btn-add-sponsor', function(e) {
    e.preventDefault();
    items_sponsors++;
    source      = $("#template-sponsors").html();
    template    = Handlebars.compile(source);
    items_sponsors = $("#list-sponsors").find('.item-sponsor').length;
    template    = template({counter: items_sponsors});
    $('#list-sponsors').append(template);
    setTimeout(function() {
        bindFileUpload();
    }, 500);
});

//Del
function deleteSponsor(este) {
    var container= este.parents('.item-sponsor:first');
    items_sponsors = $("#list-sponsors").find('.item-sponsor').length;
    if(items_sponsors <= 1) {
        container.remove();
        $('.btn-add-sponsor').click();
    } else {
        container.fadeOut(500);
        setTimeout(function() {
            container.remove();
        }, 500);
    }
}


/*************
 * ATTACHMENT
 ************/
function loadFiles(data) {
    var source      = $("#template-files").html();
    var template    = Handlebars.compile(source);
    var template    = template({files: data});
    $('#list-files').append(template);
    setTimeout(function() {
        bindFileUpload();
    }, 500);
}

$('body').on('click', '.btn-add-file', function(e) {
    e.preventDefault();
    var source          = $("#template-files").html();
    var template        = Handlebars.compile(source);
    var items           = $("#list-files").find('.item-file').length;
    var template        = template({counter: items});
    $('#list-files').append(template);
    setTimeout(function() {
        bindFileUpload();
    }, 500);
});

//Del
function deleteFile(este) {
    var container   = este.parents('.item-file:first');
    var items       = $("#list-files").find('.item-file').length;
    if(items <= 1) {
        container.remove();
        $('.btn-add-file').click();
    } else {
        container.fadeOut(500);
        setTimeout(function() {
            container.remove();
        }, 500);
    }
}


/***************
 * EVENTS - FCA
 **************/
$('body').on('click', '.fca-template li', function(){
    var este    = $(this);
    var tmp     = este.attr('data-template');
    $('.fca-template').find('.box').removeClass('active');
    este.find('.box:first').addClass('active');
    $('.fca-template').find('input[type="hidden"]:first').val(tmp);
    $('.fca-arrow').find('li').removeClass('active');
    $('.fca-arrow').find('li[data-template="'+tmp+'"]').addClass('active');
    if(tmp == 'default') {
        $('.fca-wrapper').fadeOut(100).addClass('hide');
    } else {
        $('.fca-wrapper').fadeIn(500).removeClass('hide');
        if(tmp == 'tc-ni') {
            $('.fca-image').fadeOut(500).addClass('hide');
            $('.fca-video').fadeOut(500).addClass('hide');
        } else {
            $('.fca-image').fadeIn(500).removeClass('hide');
            $('.fca-video').fadeIn(500).removeClass('hide');
        }
    }
});




/*************
 * FCA
 ***********/
//Filter
$('body').on('change', 'select[name="filter-location"]', function(e) {
    var este = $(this);
    var location = (este.val().length < 1) ? 'all' : este.val();
    $.remote({url: '/cmsadmin/fca/?location='+location, change_url: true, capa: 'view-content'});
});

//Status
function statusFCA(este) {
    var container   = este.parents('tr:first');
    var url         = este.attr('data-to');
    var redir       = (este.attr('data-redir') !== undefined) ? este.attr('data-redir') : '/cmsadmin/fca/';
    $.get(url, function(r) {
        if(r.error !== true) {
            //flashSuccess(r.msg);
            $.remote({url: redir, change_url: true, capa: 'view-content'});
        } else {
            var message = (r.msg !== undefined) ? r.msg : 'Oops! An internal server error has occurred.\nPlease try again';
            flashError(message);
        }
    }, 'json');

}

//Delete
function deleteFCA(este) {
    var container   = este.parents('tr:first');
    var url         = este.attr('data-to');
    var redir       = (este.attr('data-redir') !== undefined) ? este.attr('data-redir') : '/cmsadmin/fca/';
    $.get(url, function(r) {
        if(r.error !== true) {
            if(container.length > 0) {
                container.fadeOut(500);
                setTimeout(function() {
                    container.remove();
                }, 550);
                flashSuccess('The record has been deleted successfully');
            } else {
                $.remote({url: redir, change_url: true, capa: 'view-content'});
            }
        } else {
            var message = (r.msg !== undefined) ? r.msg : 'Oops! An internal server error has occurred.\nPlease try again';
            flashError(message);
        }
    }, 'json');

}

//Preview
$('body').on('click', '.btn-preview-fca', function(e) {
    e.preventDefault();
    var este = $(this);
    $('#modal-preview-fca').foundation('reveal', 'open');
});

