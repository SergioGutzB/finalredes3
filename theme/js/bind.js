// Foundation JavaScript
bindElements();

function bindElements() {
    $("body").css("cursor", "wait");
    $(".js-load").css("cursor", "wait");
    setTimeout(function() {
        $(document).foundation('reflow');
        $(document).foundation({
            abide: {
                live_onblur: false,
                patterns: {
                    pint: /^[0-9]+$/,
                    zip: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                    //url: /(((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?/
                }
            }
        });
        bindTimepicker();
        bindDatepicker();
        bindWysiwyg();
        bindFileUpload();
        $(document).foundation('orbit', 'reflow');
        $("body").css("cursor", "default");
        $(".js-load").css("cursor", "");
    }, 900);
}

/*** BIND LINK ***/
$(function() {
    $('body').on('click', '.disabled', function(e) {
        e.preventDefault();
        return false;
    });
    // Form ajax
    $("body").on('submit', 'form.js-remote', function(event) {
        event.preventDefault();
        var este = $(this);
        var val = true;
        var button = $('[type=submit]', este);
        button.attr('disabled', 'disabled');
        var url = este.attr('action');
        var div = (este.attr('data-to') == undefined) ? 'view-content' : este.attr('data-to');
        var before_send = este.attr('before-send');
        var after_send = este.attr('after-send');
        if (before_send != undefined) {
            try {
                val = eval(before_send);
            } catch (e) {
            }
        }
        if (!val) {
            button.removeAttr('disabled');
            return false;
        }
        $.post(url, este.serialize(), function(data, status) {
            var capa = $('#' + div);
            if (after_send != null) {
                try {
                    eval(after_send);
                } catch (e) {
                }
            }
            capa.html(data).hide().fadeIn(500);
            if (div == 'view-content') {
                $("html, body").animate({scrollTop: 0}, 500);
            }
            button.attr('disabled', null);
        });
    });

    //Confirm
    $('body').on('click', '.js-confirm', function(e) {
        e.preventDefault();
        var este    = $(this);
        if(este.hasClass('disabled')) {
            return false;
        }
        jsConfirm(este);
    });

    //Close modal
    $('body').on('click', '.close-modal', function(e) {
        e.preventDefault();
        $(this).parents('.reveal-modal').first().find('.close-reveal-modal').click();
    });

});

function jsConfirm(este) {
    var dialog      = $("#modal-js-confirm");
    var data_body   = (este.attr('data-msg') === undefined) ? 'Are you sure?' : este.attr('data-msg');
    var data_title = (este.attr('data-title') === undefined) ? 'Confirm' : este.attr('data-title');
    if ($("#modal-js-confirm").size() > 0) {
        dialog.empty();
    } else {
        dialog = $('<div id="modal-js-confirm" class="reveal-modal onend small" data-reveal>');
        $('body').append(dialog);
    }
    dialog.addClass('small');
    dialog.addClass('reveal-modal-confirm');
    var header  = $('<div class="reveal-modal-title"><i class="fa fa-warning" style="padding-right:15px; margin-top:5px;"></i>' + data_title + '</div>');
    var body    = $('<p>' + data_body + '</p>');
    var footer  = $('<a class="close-reveal-modal"><!--CLOSE--><i class="fa fa-times"></i></a>');
    var option  = $('<div class="reveal-modal-buttons"><ul class="button-group"><li class=""><a href="" class="button tiny secondary close-modal round">Cancel</a><button class="button tiny button-primary round">Accept</button></li></div>');

    dialog.append(header);
    dialog.append(body);
    dialog.append(footer);
    dialog.append(option);

    $('.button-primary', dialog).on('click', function() {
        $('#modal-js-confirm').foundation('reveal', 'close');
        if ($(this).hasClass('button-primary')) {
            if (este.attr('data-confirm') != undefined) {
                fn = este.attr('data-confirm') + '(este)';
                eval(fn);
                return false;
            }
        }
    });

    $('a.close-modal', dialog).on('click', function(e) {
        e.preventDefault();
        $('#modal-js-confirm').foundation('reveal', 'close');
    });

    $('#modal-js-confirm').foundation('reveal', 'open');
    $(document).on('open', '#modal-js-confirm', function() {
        $('.secondary', dialog).focus();
    });

}



/***************
 * BIND ELEMENTS
 ***************/

/*** Bind "TinyMCE" ***/
function bindWysiwyg() {

    $('.tiny-full').each(function() {
        var este    = $(this);
        var height  = parseInt(este.css('height'));
        if($(this).parents('div.reveal-modal:first').size() === 0 || $(this).parents('div.reveal-modal:first').hasClass('open')) {
            if(!$(this).hasClass('wysiwyg-loaded')) {
                $(this).addClass('wysiwyg-loaded');
                $(this).tinymce({
                    plugins: ["link textcolor hr colorpicker textpattern paste" ],
                    setup: function (editor) { editor.on('change', function () { tinymce.triggerSave(); }); },
                    menubar: false,
                    paste_as_text: true,
                    skin : 'amtrak',
                    height : height,
                    force_br_newlines : true,
                    force_p_newlines : false,
                    forced_root_blocks: false,
                    toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link forecolor",
                    fontsize_formats: "0.675rem 0.75rem 0.875rem 1rem 1.125rem 1.2rem 1.3rem 1.5rem 1.75rem 2rem 2.25rem 2.5rem"
                });
            }
        }
    });

    $('.tiny-fca').each(function() {
        var este    = $(this);
        var height  = parseInt(este.css('height'));
        if($(this).parents('div.reveal-modal:first').size() === 0 || $(this).parents('div.reveal-modal:first').hasClass('open')) {
            if(!$(this).hasClass('wysiwyg-loaded')) {
                $(this).addClass('wysiwyg-loaded');
                $(this).tinymce({
                    plugins: ["link textcolor colorpicker textpattern paste" ],
                    setup: function (editor) { editor.on('change', function () { tinymce.triggerSave(); }); },
                    menubar: false,
                    statusbar: false,
                    paste_as_text: true,
                    skin : 'amtrak',
                    height : height,
                    force_br_newlines : true,
                    force_p_newlines : false,
                    forced_root_blocks: false,
                    toolbar1: "bold italic | forecolor",
                    fontsize_formats: "0.675rem 0.75rem 0.875rem 1rem 1.125rem 1.2rem 1.3rem 1.5rem 1.75rem 2rem 2.25rem 2.5rem"
                });
            }
        }
    });

}

/*** Bind "Datepicker" ***/
function bindDatepicker() {
    $('.date').fdatepicker({format: 'yyyy/mm/dd'}).on('changeDate', function(ev) {
        var container = $(this);
        if (container.find('.checkin').size() > 0) {
            var input_checkin = container.find('.checkin');
            var c_co = container.find('.checkout').parent('div');
            if (c_co.size() == 0) {
                c_co = container.parents('.row:first').next();
            }
            var input_checkout = c_co.find('.checkout:first');
            if (input_checkout.size() > 0) {
                if (input_checkout.val().length == 10) {
                    var tmp_date_ci = input_checkin.val().split('/');
                    var tmp_date_co = input_checkout.val().split('/');
                    var checkin = new Date(tmp_date_ci[2], tmp_date_ci[0], tmp_date_ci[1], 0, 0, 0, 0);
                    var checkout = new Date(tmp_date_co[2], tmp_date_co[0], tmp_date_co[1], 0, 0, 0, 0);
                    if (checkin.valueOf() > checkout.valueOf()) {
                        setTimeout(function() {
                            input_checkin.attr('data-invalid', '').val('').parent('div').addClass('error');
                        }, 500);
                    } else {
                        input_checkin.removeAttr('data-invalid').parent('div').removeClass('error');
                    }
                }
            }
        } else if (container.find('.checkout').size() > 0) {
            var input_checkout = container.find('.checkout');
            var c_ci = container.parents('.row:first');
            if (c_ci.find('.checkin').size() == 0) {
                c_ci = c_ci.prev('.row');
            }
            var input_checkin = c_ci.find('.checkin:first');
            if (input_checkin.size() > 0) {
                if (input_checkin.val().length == 10) {
                    var tmp_date_ci = input_checkin.val().split('/');
                    var tmp_date_co = input_checkout.val().split('/');
                    var checkin = new Date(tmp_date_ci[2], tmp_date_ci[0], tmp_date_ci[1], 0, 0, 0, 0);
                    var checkout = new Date(tmp_date_co[2], tmp_date_co[0], tmp_date_co[1], 0, 0, 0, 0);
                    if (checkin.valueOf() > checkout.valueOf()) {
                        setTimeout(function() {
                            input_checkout.attr('data-invalid', '').val('').parent('div').addClass('error');
                        }, 500);
                    } else {
                        input_checkout.removeAttr('data-invalid').parent('div').removeClass('error');
                    }
                } else {
                    setTimeout(function() {
                        input_checkout.attr('data-invalid', '').val('').parent('div').addClass('error');
                    }, 500);
                }
            }
        }
    });
}

/*** Bind "TimePicker" ***/
function bindTimepicker() {
    $('.input-timepicker').each(function() {
        if(!$(this).hasClass('timepicker-loaded')) {
            $(this).timepicker({'step': 30, 'timeFormat': 'h:i A'});
            $(this).addClass('timepicker-loaded');
        }
    })
}


/*** Bind "FileUpload" ***/
function bindFileUpload() {
    var files = $('.js-upload');
    files.each(function() {
        var dropZone    = $(this).parents('.file-upload:first');
        var accepted    = $(this).attr('accept');
        $(this).fileupload({
            dataType: 'html',
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 11000000,
            dropZone: dropZone,
            add: function (e, data) {
                //Validate Type
                var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
                //Validate Input
                if(data.originalFiles[0]['type'] !== undefined) {
                    if(accepted != '*') {
                        if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                            flashError('The filetype you are attempting to upload is not allowed.');
                            return false;
                        }
                    }
                    //Validate Size
                    var max_size    = ($(this).attr('max-file-size') == undefined) ? 10 : $(this).attr('max-file-size');
                    var size        = data.originalFiles[0]['size'];
                    if(size > (1024 * 1024 * parseInt(max_size))) {
                        flashError('The file you are attempting to upload is larger than the permitted size ('+parseInt(max_size)+'M).');
                        return false;
                    }
                }
                //Start Progress Bar
                startProgressBar($(this));
                //Send file
                data.submit();
            },
            progressall: function (e, data) {
                //Progress
                var progress    = parseInt(data.loaded / data.total * 100, 10);
                updateProgressBar($(this), progress);
            },
            fail: function (e, data) {
                //Show error
                flashError('The file could not be uploaded to the server.\nPlease try again');
                endProgressBar($(this));

            },done: function (e, data) {
                //Container
                var container   = $(this).parents('.file-upload:first');
                //Input
                var input       = $(this);

                //Result
                try {

                    var tmp     = data.result.replace('<pre>', '').replace('</pre>', '');
                    var r       = JSON.parse(tmp);
                    if (r.error === false) {
                        //Set value "name" into input type hidden
                        var form        = input.parents('form:first');
                        //Get input name
                        var iName       = (container.attr('data-input') !== undefined) ? container.attr('data-input') : input.attr('data-input');
                        //Get input "hidden"
                        var iHidden     = $("input[name='" + iName + "']", form);
                        //Set response
                        iHidden.val(r.name);
                        if(input.attr('after-upload') !== undefined) {
                            fn = input.attr('after-upload') + '(r, input)';
                            eval(fn);
                        } else {
                            //Check link
                            var link    = container.find('.file-upload-link');
                            if(link.size() > 0 ) {
                                link.find('a:first').text(r.name).attr('href', r.source+r.name);
                                var button = container.find('.file-upload-button');
                                button.fadeOut(500);
                                setTimeout(function() {
                                    link.hide().removeClass('hide').fadeIn(500);
                                }, 500);
                                //$("[value=Save]").click();
                            } else {
                                //Show image
                                container.find('img').attr('src', r.source+r.name);
                            }
                        }
                    } else {
                        flashError((r.message !== undefined) ? r.message : 'The file could not be uploaded to the server.\nPlease try again. 2');
                    }

                } catch(e) {
                    flashError('The file could not be uploaded to the server. Please try again.');
                }

                endProgressBar($(this));
            }
        });
    });
}

//Start Progress Bar
function startProgressBar(input) {
    var container   = input.parents('.file-upload:first');
    var bar         = container.find('.progress-info');
    if((bar.size() > 0)) {
        bar.removeClass('hide').show().find('.progress').removeClass('alert');
        bar.find('.meter:first').css('width', '0%');
    } else {
         container.find('span.upload').text('Loading...').addClass('disabled');
    }
    input.attr('disabled', 'disabled');
}
//Update Progress Bar
function updateProgressBar(input, progress) {
    var container   = input.parents('.file-upload:first');
    var bar         = container.find('.progress-info');
    if(bar.size() > 0) {
        bar.find('.meter:first').css('width', progress+'%');
    } else {
        container.find('span.upload').text('Loading... '+progress+'%').addClass('disabled');
    }
}
//End Progress bar
function endProgressBar(input, alert) {
    var container   = input.parents('.file-upload:first');
    var icon        = (container.attr('data-icon') === undefined) ? 'fa-plus' : container.attr('data-icon');
    var text        = (container.attr('data-text') === undefined) ? 'Upload' : container.attr('data-text')
    var bar         = container.find('.progress-info');
    if(bar.size() > 0) {
        bar.fadeOut(700);
    } else {
        container.find('span.upload').html('<i class="fa '+icon+' fa-pd-right"></i>'+text).removeClass('disabled');
    }
    input.removeAttr('disabled');
}
//Delete Image
function deleteImage(este) {
    var value   = (este.attr('data-file') !== undefined) ? este.attr('data-file') : '';
    var source  = (este.attr('data-source') !== undefined) ? este.attr('data-source') : '';
    var c       = este.parents('.file-upload:first');
    c.find(':hidden').val('');
    c.find('img').attr('src', source+value);
}

//Delete File
function deleteAttachment(este) {
    var c       = este.parents('.file-upload:first');
    c.find(':hidden').val('');
    c.find('.file-upload-link').fadeOut(200);
    c.find('.file-upload-link').find('a:first').text('').attr('href', '');
    setTimeout(function() {
        c.find('.file-upload-button').hide().removeClass('hide').fadeIn(200);
    }, 200)
}
