/*
 JUMPYFIELD is a simple little thing that makes the focus jump between fields when they reach maxlength.
 Usage:

 $(".myInputFields").jumpy();

 jumpy also accepts an optional array of default values that count the fields as "empty". Example:
 $(".myInputFields").jumpy(["YY","MM","DD"]);
 */
var jumpEnabled = true;

$.fn.jumpy = function () {
    // Save the context in the element so that we know which fields to jump between.
    this.data("_jumpySelector", this.selector);
    this.keyup(function (event) {
        var tabKeyCode = 9;
        if (event.keyCode == tabKeyCode) {
            // Ignore accidental tabbing as a result of the user not knowing about the
            // jumpy fields, and trying to tab after filling out a field.
            var previousJumpy = $(this)._getPreviousJumpyField();
            var isEmpty = $(previousJumpy).val().length == 0;
            var hasDefaultValue = $(previousJumpy).data("_defaultValue") == $(previousJumpy).val();
            if (isEmpty || hasDefaultValue)
                previousJumpy.focus().select();

            return false;
        }
        if (jumpEnabled && ($(this).val().length >= parseInt($(this).attr("maxlength")))) {
            var nextjf = $(this)._getSubsequentJumpyField();
            $(nextjf).focus().select();
            jumpEnabled = false;
            // Wait a short while here, otherwise it will jump right on to the next field
            setTimeout(function () {
                jumpEnabled = true;
            }, 50);
        }
    });
};

$.fn._getSubsequentJumpyField = function () {
    var lastIterated = null;
    var subsequent = null;
    var originator = this[0];
    var selector = $(this).data("_jumpySelector");
    $(selector).each(function (index, iterated) {
        if (subsequent == null) {
            if (lastIterated == originator) {
                subsequent = this;
            }
            lastIterated = this;
        }
    });
    return subsequent;
}

$.fn._getPreviousJumpyField = function () {
    var lastIterated = null;
    var previous = null;
    var originator = this[0];
    var selector = $(this).data("_jumpySelector");
    $(selector).each(function (index, iterated) {
        if (previous == null) {
            if (this == originator) {
                previous = lastIterated;
            }
            lastIterated = this;
        }
    });

    return previous;
}