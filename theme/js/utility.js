// Foundation JavaScript
$(document).foundation({ abide : { live_onblur: false } });


/**
 * Required:
 *
 * oauth/facebook.js
 *
 */

/** Function to display messages manually ***/
function flashValid(msg, delay) { return flashShow(msg, 'success', delay); }
function flashSuccess(msg, delay) { return flashValid(msg, delay); }
function flashWarning(msg, delay) { return flashShow(msg, 'warning', delay); }
function flashInfo(msg, delay) { return flashShow(msg, 'info', delay); }
function flashError(msg, delay) { return flashShow(msg, 'alert', delay); }
function flashShow(msg, type, delay) { var tmp_id = Math.floor(Math.random()*11); if(delay===undefined) { delay = 7000; } $(".flash-message").empty(); $('.flash-message:first').append('<div id="alert-id-'+tmp_id+'" data-alert class="alert-box radius ' + type + '">'+msg+'<a href="#" class="close">&times;</a></div><script type="text/javascript">if('+delay+' > 0) { $("#alert-id-'+tmp_id+'").delay('+delay+').fadeOut(500); } else { $("#alert-id-'+tmp_id+'").show(); }</script>'); }
function flashClear() { $(".flash-message").empty(); }
function showInputError(elem, timeout) { setTimeout(function() { elem.attr('data-invalid', '').parent('div').addClass('error'); }, (timeout > 0) ? timeout : 700); }
function removeInputError(elem, timeout) { setTimeout(function() { elem.removeAttr('data-invalid').parent('div').removeClass('error'); }, (timeout > 0) ? timeout : 700); }
$(function() { $('body').on('click', '.flash-message a.close', function(e) { e.preventDefault(); $(this).parents('.alert-box:first').hide(); }); });

/*** Ajax utility ***/
$(document).ajaxStart(function() { $("body").css("cursor", "wait"); }).ajaxStop(function() { $(document).foundation(); $("body").css("cursor", "default"); if (typeof bindElements === 'function') { bindElements(); } });

/* Notify form invalid */
$('body').on('invalid', 'form', function() {
    var input_error = $('form').find('[aria-invalid="true"]').first();
    input_error.focus();
    console.log(input_error.attr('name'));
});

/***
 * Event Tracking
 *
 * attr: data-ga-category (Category)
 * attr: data-ga-event  (Event Name)
 */
$(function() { $('body').on('click', '.ga-tracking', function(e) { if (typeof gaEvent === 'function') {  var este    = $(this); gaEvent(este.attr('data-ga-category'), este.attr('data-ga-event')); } }); });

/**
 * Open Panels
 * @param string panel
 * @param string container
 */
function openPanel(panel, container) {
    var panel   = (container === undefined) ? $('.panel.'+panel) : $('#'+container).find('.panel.'+panel);
    if(panel.attr('data-ga-exclude') === undefined) {
        //Event Tracking
        if (typeof gaEvent === 'function') {
            try { gaEvent('Open Panel', panel); } catch(e) { }
        }
    }
    if(container === undefined) {
        $('.panel').addClass('hide').hide();
        panel.removeClass('hide').show();
        $('html,body').animate({scrollTop: panel.offset().top}, 'slow');
    } else {
        container = $('#'+container);
        container.find('.panel').addClass('hide').hide();
        panel.removeClass('hide').show();
        $('html,body').animate({scrollTop: container.offset().top}, 'slow');
    }
}

/*******************
 * Social Links
 ******************/
$('body').on('click', '.social-link', function(e){
    e.preventDefault();
    var este    = $(this);
    var destiny = este.attr('href');
    if(este.hasClass('social-facebook') && (typeof(FB) !== undefined)) {
        var url = window.location;
        var obj = {
            method: 'feed',
            name: este.attr('data-title'),
            picture: (este.attr('data-picture') !== undefined) ? este.attr('data-picture') : url.protocol+'//'+url.hostname+'/theme/img/cv_share_fb.jpg',
            link: (este.attr('data-link') !== undefined) ? este.attr('data-link') : url.protocol+'//'+url.hostname,
            description: este.attr('data-description')
        };
        $.facebook.share(obj);
    } else {
        window.open(destiny, 'share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
    }
});

/**
 * Valid age date
 * @param int birthMonth
 * @param int birthDay
 * @param int birthYear
 * @returns int
 */
function ageGate(birthMonth, birthDay, birthYear) {
    var age = 0;
    var minimun_age = 21;
    var todayDate = new Date();
    if (birthMonth > 12 || birthDay > 31) {
        return age;
    }
    if(birthMonth < 1 || birthDay < 1 || birthYear < 1 || birthMonth === '00' || birthDay === '00' || birthYear === '0000') {
        return age;
    }
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();
    age = todayYear - birthYear;
    if (todayMonth < birthMonth - 1) {
        age--;
    }
    if (birthMonth - 1 === todayMonth && todayDay < birthDay) {
        age--;
    }
    return (age < minimun_age) ? 0 : age;
}

//Hide label and show placeholder
$(function () { $.support.placeholder = false; test = document.createElement('input'); if('placeholder' in test) $.support.placeholder = true; if ($.support.placeholder) { $('form').find('label.to-placeholder').hide(); } });

/***
 * Checkbox readonly
 */
$('body').on('click', 'input[type="checkbox"]', function(e) { var este = $(this); if(este.attr('readonly') !== undefined) { e.preventDefault();} });