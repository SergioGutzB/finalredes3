/**
 * Funcion de Jquery para uso de ajax y json
 * @param {type} $
 * @returns {undefined}
 */

//Por si se usa el hashbang
$.prevHashBang = false;

(function($) {
    /**
     *
     * Opciones por defecto
     */
    var defaults = {
        change_url      : true, //Indica si cambia la url
        async           : false, //Indica si la petición es asíncrona
        timeout         : 45000, //Tiempo de espera        
        append_data     : false, //Indica si carga con html o append la data
        response        : 'html', //Método de respuesta que se espera
        capa            : 'view-content', //Capa a actualizar
        method          : 'GET', //Método a utilizar
        data            : null  //Data o parámetros a enviar
    };

    /**
     * Objeto para el load
     */
    $.remote = function(options) {
        //Variable de éxito (solo para peticiones no asíncronas)
        var request = false;
        //Extiendo las opciones
        var opt = $.extend(true, defaults, options);

        //Realizo la petición
        $.ajax({
            type: opt.method, url: opt.url, timeout: opt.timeout, async: opt.async, dataType: opt.response, data: opt.data,            
            error: function (xhr, text, err) {
                var response = xhr.statusCode().status+" "+xhr.statusCode().statusText;                
                alert('Oops! Something has gone wrong, please try again or refresh page: '+response);                    
                request = false;
            }
        }).success(function() {
            if(opt.response=='html') {
                $.prevHashBang = true;//Por si se utiliza el hashbang
                if(opt.change_url === true) {
                    JsUpdateUrl(opt.url);
                } else if(opt.change_url !== false) {
                    JsUpdateUrl(opt.change_url);
                }
            }
        }).done(function(data) {
            if(opt.response == 'html') {
                //Verifico si carga la data o la adhiere
                (opt.append_data==true) ? $("#"+opt.capa).append(data) : $("#"+opt.capa).html(data);                
                if(opt.capa == 'view-content') {
                    $("html, body").animate({scrollTop: 0}, 500);
                }
                request = true;
            } else {
                request = data;
            }            
        });
        
        //Reestablesco las opciones iniciales
        defaults.response = 'html';
        defaults.method = 'GET';
        defaults.data = null;

        //Retorno la variable de éxito
        //Si la petición no es asíncrona retornará un boolean o el tipo de respuesta indicado (json)
        return request;
    };
    
})(jQuery);

/** Enlazo la url **/
$(document).ready(function() {
    if (typeof window.history.pushState == 'function') {
        JsPushState();
    } else {
        JsCheckHash(); JsHashChange();
    }
});

/**
* Función que actualiza la url con popstate, hashbang o normal
*/
function JsUpdateUrl(url) {    
    url = ltrim(url, '/');
    if(typeof window.history.pushState == 'function') {
        url = '/'+url;
        history.pushState({ path: url }, url, url);
    } else {
        window.location.hash = "#!/"+url;
    }
    return true;
}

/**
 * Función que cambia la url, si el navegador lo soporta
 */
function JsPushState(){
    // Función para enlazar cuando cambia la url de la página.
    $(window).bind('popstate', function(event) {        
        //if (!event.originalEvent.state)
            //return;
        $.remote({url: location.pathname});
    });
}

/**
 * Función que verifica el hash, se utiliza cuando no soporta el popstate
 */
function JsCheckHash(){
    var direccion = ""+window.location+"";
    var nombre = direccion.split("#!/");
    if(nombre.length > 1 && nombre[1].length > 0){
        direccion = '/'+ltrim(nombre[1], '/');
        $.remote({url: direccion});
    }
}
/**
 * Función que cambia actualiza el content cuando cambia el hash
 */
function JsHashChange() {
    // Función para determinar cuando cambia el hash de la página.
    $(window).bind("hashchange",function(event) {
        if($.prevHashBang) {
            $.prevHashBang = false;
            return;
        }
        var hash = ""+window.location.hash+"";
        hash = hash.replace("#!/","");
        if(hash && hash!="") {
            $.remote({url: hash});
        }
    });
}

/**
 * Funciones para limpiar caracteres al igual que el trim de php
 */
function ltrim(str, opt){    
    if(opt) {
        while (str.charAt(0) == opt)
            str = str.substr(1, str.length - 1);
    } else {
        while (str.charAt(0) == " ")
            str = str.substr(1, str.length - 1);
    }
    return str;
}
function rtrim(str, opt){      
    if(opt) {
        while (str.charAt(str.length - 1) == opt)
            str = str.substr(0, str.length - 1);
    } else {
        while (str.charAt(str.length - 1) == " ")
            str = str.substr(0, str.length - 1);
    }
    return str;
}
function trim(str, opt){ 
    var str = new String(str);
    return rtrim(ltrim(str, opt), opt); 
}