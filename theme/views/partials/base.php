<?php
$view = (empty($view)) ? 'dashboard' : $view;

$this->load->view(THEME_VIEWS.'/partials/header');
$this->load->view(THEME_VIEWS."/$view");
$this->load->view(THEME_VIEWS.'/partials/footer');

