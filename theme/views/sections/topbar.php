<nav class="top-bar" data-topbar role="navigation">
    <ul class="title-area">
        <li class="name">
            <a href="/" ><h3>Final Redes 3</h3></a>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>
    <section class="top-bar-section">
        <ul class="left">
            <?php
            $ci = & get_instance();
            $action = $ci->router->fetch_method();
            ?>
            <li class="<?= ($action == 'index') ? 'active' : ''; ?>"><a href="/mail/" >Servicio - Mail</a></li>
            <li class="<?= ($action == 'users') ? 'active' : ''; ?>"><a href="/ftp/" >Servicio - FTP</a></li>
        </ul>
        <ul class="right inline-list">
            <li><a href="/cmsadmin/sessions/logout">Logout</a></li>
        </ul>
    </section>
</nav>