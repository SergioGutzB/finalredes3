<div class="login panel">
  <div class="row">
    <div class="small-11 medium-6 small-centered columns">
      <form id="form-login" data-abide>
        <div class="row">
          <div class="small-12 columns">
            <label>Nombre de usuario</label>
            <input type="text" required>
            <small class="error">Ingrese un usuario valido</small>
          </div>

          <div class="small-12 columns">
            <label>Contraseña</label>
            <input type="password" required>
            <small class="error">Ceontraseña incorrecta</small>
          </div>

          <div class="small-12 columns">
            <div class="text-center">
              <button type="submit" class="button">Iniciar Sesión</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>